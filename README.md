# Express + Preact + Typescript + ESBuild Template

This is a template which includes Express, Preact, Typescript, and ESBuild.

## How to Develop

1. Clone this repository
2. With powershell, run ./install.bat to install npm packages
3. Then, run ./start.bat to start nodemon servers

NOTE: Once you use ./install.bat, you don't need it anymore. So feel
free to remove it.

### If you don't want to use the scripts

1. Clone this repository
2. Go to the client folder and (npm install)
3. Go to the server folder and (npm install)
4. Open two terminals
5. With the first terminal, Go to the client folder and (npm run watch)
6. With the other terminal, Go to the server folder and (npm run watch)
