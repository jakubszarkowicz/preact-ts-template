import React, { memo } from "preact/compat";
import { render } from "preact";
 
const App = memo(() => {
    return (
        <main>
            Hello, Preact!
        </main>
    );
})

render(<App />, document.body);