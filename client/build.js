require("esbuild")
    .build({
        entryPoints: ["src/App.tsx"],
        bundle: true,
        outfile: "public/bundle.js",
    })
    .catch(() => process.exit(1));
