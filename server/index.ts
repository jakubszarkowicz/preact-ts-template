import express from "express";

const app = express();

app.use(express.static("../client/public"));
app.use(express.json());

app.listen(8080, () => {
    console.log(`Listening on http://localhost:8080`);
});
